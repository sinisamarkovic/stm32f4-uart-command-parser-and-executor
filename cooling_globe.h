#ifndef __COOLING_GLOBE__
#define __COOLING_GLOBE__

#include "stm32f4xx.h"
#include "stdint.h"

typedef struct cooling_globe_t
{
	uint16_t time;				// x1s
	uint16_t temp;				// x0.1 C = in tens of degrees 12.5C -> 125 value
} COOLING_GLOBE_INFO;


#define COOLING_GLOBE_ARRAY_SIZE						113

extern const COOLING_GLOBE_INFO c_COOLING_GLOBE_INFO[COOLING_GLOBE_ARRAY_SIZE];


#endif
