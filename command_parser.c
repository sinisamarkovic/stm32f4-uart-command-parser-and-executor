#include "command_parser.h"
#include <stdio.h>
#include <stdarg.h>
#include "stm32f4xx.h"
#include "misc.h"
#include "usart.h"
#include "delay.h"
#include <string.h>
#include <errno.h>
#include "dac.h"
#include "adc.h"
#include "math.h"
#include <stdlib.h>
#include "cooling_globe.h"

volatile uint8_t g_command_buffer[COMMAND_BUFFER_SIZE];
volatile uint32_t g_led0_off_interval = 0;
volatile uint32_t g_led0_on_interval = 0;
volatile uint16_t g_led0_flag = 0;
volatile uint32_t g_led0_timer = 0;
volatile uint32_t g_led1_off_interval = 0;
volatile uint32_t g_led1_on_interval = 0;
volatile uint16_t g_led1_flag = 0;
volatile uint32_t g_led1_timer = 0;
volatile uint32_t g_led2_off_interval = 0;
volatile uint32_t g_led2_on_interval = 0;
volatile uint16_t g_led2_flag = 0;
volatile uint32_t g_led2_timer = 0;
volatile uint32_t g_led3_off_interval = 0;
volatile uint32_t g_led3_on_interval = 0;
volatile uint16_t g_led3_flag = 0;
volatile uint32_t g_led3_timer = 0;
//parsing
struct Command parse_command()
{
  struct Command command;
  char delimiter[] = " ";
  char* token = strtok(g_command_buffer, delimiter);
  if(!strcmp(token,"pbtn"))
  {
    if(strtok(NULL,delimiter) == NULL)
    {
      command.type = PUSHBUTTONSTATUS;
    }
    else
    {
      command.type = INVALIDCOMMAND;
    }
  }
  else if(!strcmp(token,"help"))
  {
    if(strtok(NULL,delimiter) == NULL)
    {
      command.type = HELP;
    }
    else
    {
      command.type = INVALIDCOMMAND;
    }
  }
  else if(!strcmp(token,"led"))
  {
    parse_led_command(&command);
  }
  else if(!strcmp(token,"dac1"))
  {
    parse_dac_command(&command);
  }
  else if(!strcmp(token,"adc1"))
  {
    parse_adc_command(&command);
  }
  else if(!strcmp(token,"cool"))
  {
    parse_cool_command(&command);
  }
  else
  {
    command.type = INVALIDCOMMAND;
  }
  return command;
}

void parse_cool_command(struct Command* command_ptr)
{
  char delimiter[] = " ";
  char* token = strtok(NULL, delimiter);
  if(token == NULL)
  {
    command_ptr->type = INVALIDCOMMAND;
    return;
  }
  uint16_t t = strtol(token, NULL, 10);
  if(errno == ERANGE)
  {
    command_ptr->type = INVALIDCOMMAND;
    return;
  }
  if(t > 630)
  {
    command_ptr->a1 = 50;
    command_ptr->a2 = 50;
    command_ptr->f1 = 100;
    command_ptr->f2 = 100;
    command_ptr->type = INVALIDCOMMAND;
    return;
  }
  token = strtok(NULL,delimiter);
  if(token == NULL)
  {
    command_ptr->type = COOL;
    command_ptr->t = t;
    return;
  }
  else
  {
    command_ptr->type = INVALIDCOMMAND;
    return;
  }
}

void parse_adc_command(struct Command* command_ptr)
{
  char delimiter[] = " ";
  char* token = strtok(NULL, delimiter);
  if(token == NULL)
  {
    command_ptr->type = INVALIDCOMMAND;
    return;
  }
  if(!strcmp(token,"r"))
  {
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = ADCSET;
    }
    else
    {
      command_ptr->type = INVALIDCOMMAND;
    }
  }
  else if(!strcmp(token,"s"))
  {
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = ADCOFF;
    }
    else
    {
      command_ptr->type = INVALIDCOMMAND;
    }
  }
}

void parse_dac_command(struct Command* command_ptr)
{
  char delimiter[] = " ";
  char* token = strtok(NULL, delimiter);
  if(token == NULL)
  {
    command_ptr->type = INVALIDCOMMAND;
    return;
  }
  if(!strcmp(token,"s"))
  {
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = DACOFF;
      return;
    }
    uint16_t f1 = strtol(token,NULL,10);
    if((f1 < 10) || (errno == ERANGE) || (f1 > 10000))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    float a1 = strtof(token,NULL);
    if((errno == ERANGE))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    uint16_t f2 = strtol(token,NULL,10);
    if((f2 < 10) || (errno == ERANGE) || (f2 > 10000))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    float a2 = strtof(token,NULL);
    if((errno == ERANGE))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    if(!(abs(a1) + abs(a2) <= 0.49))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->a1 = a1;
      command_ptr->a2 = a2;
      command_ptr->f1 = f1;
      command_ptr->f2 = f2;
      command_ptr->type = DACSET;
      return;
    }
    else
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
  }
  else if(!strcmp(token,"t"))
  {
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    uint16_t f = strtol(token,NULL,10);
    if((f < 10) || (errno == ERANGE) || (f > 10000))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    float a = strtof(token,NULL);
    if((errno == ERANGE) || (a < 0) || (a > 3))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->a1 = a;
      command_ptr->f1 = f;
      command_ptr->type = DACTRIANGULARSET;
      return;
    }
    else
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }

  }
  else
  {
    command_ptr->type = INVALIDCOMMAND;
    return;
  }
}

void parse_led_command(struct Command* command_ptr)
{
  char delimiter[] = " ";
  char* token = strtok(NULL, delimiter);
  if(token == NULL)
  {
    command_ptr->type = INVALIDCOMMAND;
    return;
  }
  if(!strcmp(token,"s"))
  {
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    uint8_t which_led = strtol(token,NULL,10);
    if((which_led > 3) || (errno == ERANGE))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    uint8_t static_state = strtol(token,NULL, 10);
    if((static_state > 1) || (errno == ERANGE))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token != NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    command_ptr->type = LEDSTATICSET;
    command_ptr->static_state = static_state;
    command_ptr->which_led = which_led;
  }
  else if(!strcmp(token,"b"))
  {

    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    uint8_t which_led = strtol(token, NULL, 10);
    if((which_led > 3) || (errno == ERANGE))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    uint32_t on_interval = strtol(token, NULL, 10);
    if((errno == ERANGE))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    uint32_t entire_interval = strtol(token, NULL, 10);
    if((errno == ERANGE) || entire_interval < on_interval)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token != NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    command_ptr->type = LEDBLINKSET;
    command_ptr->which_led = which_led;
    command_ptr->entire_interval = entire_interval;
    command_ptr->on_interval = on_interval;
  }
  else if(!strcmp(token,"p"))
  {
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    uint8_t which_led = strtol(token,NULL, 10);
    if((which_led > 3) || (errno == ERANGE))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token == NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    uint8_t duty_cycle = strtol(token,NULL, 10);
    if((duty_cycle > 100) || (errno == ERANGE))
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    token = strtok(NULL,delimiter);
    if(token != NULL)
    {
      command_ptr->type = INVALIDCOMMAND;
      return;
    }
    command_ptr->type = LEDPWMSET;
    command_ptr->duty_cycle = duty_cycle;
    command_ptr->which_led = which_led;
  }
  else
  {
    command_ptr->type = INVALIDCOMMAND;
  }
}

void execute_command(struct Command* command_ptr)
{
  if(command_ptr->type == PUSHBUTTONSTATUS)
  {
    uint8_t push_button_status = GPIOA->IDR & 0x0001;
    printUSART2("Push button status = %d\n", push_button_status);
  }
  else if(command_ptr->type == LEDSTATICSET)
  {
    execute_led_static_set(command_ptr);
  }
  else if(command_ptr->type == LEDBLINKSET)
  {
    execute_led_blink_set(command_ptr);
  }
  else if(command_ptr->type == LEDPWMSET)
  {
    execute_led_pwm_set(command_ptr);
  }
  else if(command_ptr->type == HELP)
  {
    printUSART2("pbtn - shows push button status\nled s N S - sets led N to state S, example : led s 1 1 sets led 1 to state 1.\nled b N O P - makes led N blink with on interval = O and off interval = P - O, example : led b 1 500 1000 makes led 1 blink with on time = 500ms and off time = 500ms.\nled p N D - enables pwm on led N with duty cycle = D(0-100) and PWM period = 1ms, example : led p 1 50 enables pwm on led 1 with 50 duty cycle.\nhelp - shows all available commands.\ndac1 s f1 a1 f2 a2 initializes DAC1 on pin PA4 with sampling frequency = 40 kHz DMA and generates a linear combination of two sinuses in the shape of : x(t) = a1sin(2*pi*f1*t) + a2(2*pi*f2*t) + 0.5 and abs(a2) + abs(a2) needs to be <= 0.49. Frequencies need to be in the range 10Hz-10kHz.\ndac1 s - stops dac.\nadc1 r initializes adc with dma.\nadc1 s - stops adc.\ndac1 t f a - dac1 on PA4 pin with sampling rate = 40kHz, generates triangular wave with frequncy f and amplitude a, frequency needs to be in range 10Hz-10kHz, and amplitude from 0 - 3.\ncool t shows temperature for time istance t, if there is no measurement the temp is approximated, t needs to be in the range 1-630.\n");
  }
  else if(command_ptr->type == DACSET)
  {
    execute_dac_set(command_ptr);
  }
  else if(command_ptr->type == DACOFF)
  {
    DAC->CR &= ~DAC_CR_EN1;
    g_dac_set = 0;
    printUSART2("DAC disabled on pin PA4.\n");
  }
  else if(command_ptr->type == ADCSET)
  {
	  ADC1->CR2 |= ADC_CR2_ADON;
    g_adc_set = 1;
    printUSART2("ADC enabled on pin PA1.\n");
  }
  else if(command_ptr->type == ADCOFF)
  {
	  ADC1->CR2 &= ~ADC_CR2_ADON;
    g_adc_set = 0;
    printUSART2("ADC disabled on pin PA1.\n");

  }
  else if(command_ptr->type == DACTRIANGULARSET)
  {
    getData4DAC(g_dac_buff, 1, command_ptr->a1, 0, command_ptr->f1, 0);
    g_dac_set = 1;
    DAC->CR |= DAC_CR_EN1;												// 
    printUSART2("Dac enabled on pin PA4, producing triangular wave with a = %f and f = %d.\n", command_ptr->a1, command_ptr->f1);
  }
  else if(command_ptr->type == COOL)
  {
    execute_cool(command_ptr);
  }
  else if(command_ptr->type == INVALIDCOMMAND)
  {
    if(command_ptr->a1 == 50 && command_ptr->a2 == 50 && command_ptr->f1 == 100 && command_ptr->f2 == 100)
    {
      printUSART2("Value is out of scope.\n");
      return;
    }
    printUSART2("ERROR: unknown command.\n");
  }
  else
  {
    printUSART2("Something went really wrong.\n");
  }
}

void execute_cool(struct Command* command_ptr)
{
    //COOLING_GLOBE_INFO c_COOLING_GLOBE_INFO[
  uint8_t found = 0;
  uint16_t t = command_ptr->t;
  uint16_t i = 0;
  uint16_t i_before;
  uint16_t i_correct;
  uint16_t i_after;
  float temp;
  float gradient;
  for(i; i < COOLING_GLOBE_ARRAY_SIZE; ++i)
  {
    if(c_COOLING_GLOBE_INFO[i].time == t)
    {
      i_correct = i;
      found = 1;
    }
    else if(c_COOLING_GLOBE_INFO[i].time > t)
    {
      i_after = i;
      break;
    }
    else
    {
      i_before = i;
    }
  }
  if(found == 1)
  {
    temp = c_COOLING_GLOBE_INFO[i_correct].temp / 10.0;
    printUSART2("t = %d s -> T = %fC\n", t, temp);
  }
  else
  {
    float T1 = c_COOLING_GLOBE_INFO[i_before].temp / 10.0;
    float T2 = c_COOLING_GLOBE_INFO[i_after].temp / 10.0;
    uint16_t t1 = c_COOLING_GLOBE_INFO[i_before].time;
    uint16_t t2 = c_COOLING_GLOBE_INFO[i_after].time;
    temp = T1 + (T2- T1) * ((t - t1)/(t2- t1));
    gradient = (T2 - T1)/(t2 - t1);
    printUSART2("t = %d s -> T = %fC -> dT = %fC\n", t, temp, gradient);
  }
}

void execute_dac_set(struct Command* command_ptr)
{
  getData4DAC(g_dac_buff, 0, command_ptr->a1, command_ptr->a2, command_ptr->f1, command_ptr->f2);
  g_dac_set = 1;
	DAC->CR |= DAC_CR_EN1;												// 
  printUSART2("Dac enabled on pin PA4, producing sin linear combination with a1 = %f, a2 = %f, f1 = %d and f2 = %d.\n", command_ptr->a1, command_ptr->a2, command_ptr->f1, command_ptr->f2);
}

void execute_led_static_set(struct Command* command_ptr)
{
  uint16_t set_val;
  switch(command_ptr->which_led)
  {
    case 0: 
      g_led0_flag = 0;
      GPIOD->MODER |= GPIO_MODER_MODER12_0;
      GPIOD->MODER &= ~GPIO_MODER_MODER12_1;
      set_val = 0x1000;
      break;
    case 1:
      g_led1_flag = 0;
      GPIOD->MODER |= GPIO_MODER_MODER13_0;
      GPIOD->MODER &= ~GPIO_MODER_MODER13_1;
      set_val = 0x2000;
      break;
    case 2:
      g_led2_flag = 0;
      GPIOD->MODER |= GPIO_MODER_MODER14_0;
      GPIOD->MODER &= ~GPIO_MODER_MODER14_1;
      set_val = 0x4000;
      break;
    case 3:
      g_led3_flag = 0;
      GPIOD->MODER |= GPIO_MODER_MODER15_0;
      GPIOD->MODER &= ~GPIO_MODER_MODER15_1;
      set_val = 0x8000;
      break;
  }
  if(command_ptr->static_state == 0)
  {
    GPIOD->ODR &= ~set_val;
  }
  else
  {
    GPIOD->ODR |= set_val;
  }
  printUSART2("Led %d set to %d.\n", command_ptr->which_led, command_ptr->static_state);
}

void execute_led_blink_set(struct Command* command_ptr)
{
  uint32_t off_interval = command_ptr->entire_interval - command_ptr->on_interval;
  if(command_ptr->which_led == 0)
  {
    g_led0_flag = 1;
    g_led0_off_interval = off_interval * 1000;
    g_led0_on_interval = command_ptr->on_interval * 1000;
    g_led0_timer = getSYSTIMER();
    GPIOD->MODER |= GPIO_MODER_MODER12_0;
    GPIOD->MODER &= ~GPIO_MODER_MODER12_1;
  }
  else if(command_ptr->which_led == 1)
  {
    g_led1_flag = 1;
    g_led1_off_interval = off_interval * 1000;
    g_led1_on_interval = command_ptr->on_interval * 1000;
    g_led1_timer = getSYSTIMER();
    GPIOD->MODER |= GPIO_MODER_MODER13_0;
    GPIOD->MODER &= ~GPIO_MODER_MODER13_1;
  }
  else if(command_ptr->which_led == 2)
  {
    g_led2_flag = 1;
    g_led2_off_interval = off_interval * 1000;
    g_led2_on_interval = command_ptr->on_interval * 1000;
    g_led2_timer = getSYSTIMER();
    GPIOD->MODER |= GPIO_MODER_MODER14_0;
    GPIOD->MODER &= ~GPIO_MODER_MODER14_1;
  }
  else if(command_ptr->which_led == 3)
  {
    g_led3_flag = 1;
    g_led3_off_interval = off_interval * 1000;
    g_led3_on_interval = command_ptr->on_interval * 1000;
    g_led3_timer = getSYSTIMER();
    GPIOD->MODER |= GPIO_MODER_MODER15_0;
    GPIOD->MODER &= ~GPIO_MODER_MODER15_1;
  }
  printUSART2("Blink on LED %d set. On interval : %d, off interval : %d\n", command_ptr->which_led, command_ptr->on_interval, off_interval);
}

void execute_led_pwm_set(struct Command* command_ptr)
{
  TIM4->CNT = 0;
  float scaler = command_ptr->duty_cycle / (float)100;
  uint16_t duty_cycle = scaler * 1000;
  if(command_ptr->which_led == 0)
  {
    g_led0_flag = 0;
    GPIOD->MODER |= GPIO_MODER_MODER12_1;
    GPIOD->MODER &= ~GPIO_MODER_MODER12_0;
    TIM4->CCR1 = duty_cycle;
    TIM4->CCER |= (TIM_CCER_CC1E);
  }
  else if(command_ptr->which_led == 1)
  {
    g_led1_flag = 0;
    GPIOD->MODER |= GPIO_MODER_MODER13_1;
    GPIOD->MODER &= ~GPIO_MODER_MODER13_0;
    TIM4->CCR2 = duty_cycle;
    TIM4->CCER |= (TIM_CCER_CC2E);
  }
  else if(command_ptr->which_led == 2)
  {
    g_led2_flag = 0;
    GPIOD->MODER |= GPIO_MODER_MODER14_1;
    GPIOD->MODER &= ~GPIO_MODER_MODER14_0;
    TIM4->CCR3 = duty_cycle;
    TIM4->CCER |= (TIM_CCER_CC3E);
  }
  else if(command_ptr->which_led == 3)
  {
    g_led3_flag = 0;
    GPIOD->MODER |= GPIO_MODER_MODER15_1;
    GPIOD->MODER &= ~GPIO_MODER_MODER15_0;
    TIM4->CCR4 = duty_cycle;
    TIM4->CCER |= (TIM_CCER_CC4E);
  }
  printUSART2("PWM with duty cycle %d on led %d set.\n",command_ptr->duty_cycle, command_ptr->which_led);
}