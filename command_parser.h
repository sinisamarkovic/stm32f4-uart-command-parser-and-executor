#ifndef __COMMAND_PARSER_H
#define __COMMAND_PARSER_H
enum command_type
{
  PUSHBUTTONSTATUS,
  LEDSTATICSET,
  LEDBLINKSET,
  LEDPWMSET,
  DACSET,
  HELP,
  DACOFF,
  ADCSET,
  ADCOFF,
  DACTRIANGULARSET,
  COOL,
  INVALIDCOMMAND
};
struct Command
{
  enum command_type type;
  uint8_t which_led;
  uint8_t static_state;
  uint32_t on_interval;
  uint32_t entire_interval;
  uint8_t duty_cycle;
  uint16_t f1;
  uint16_t f2;
  float a1;
  float a2;
  uint16_t t;
};
#define COMMAND_BUFFER_SIZE 256
extern volatile uint8_t g_command_buffer[COMMAND_BUFFER_SIZE];
extern volatile uint32_t g_led0_off_interval;
extern volatile uint32_t g_led0_on_interval;
extern volatile uint16_t g_led0_flag;
extern volatile uint32_t g_led0_timer;
extern volatile uint32_t g_led1_off_interval;
extern volatile uint32_t g_led1_on_interval;
extern volatile uint16_t g_led1_flag;
extern volatile uint32_t g_led1_timer;
extern volatile uint32_t g_led2_off_interval;
extern volatile uint32_t g_led2_on_interval;
extern volatile uint16_t g_led2_flag;
extern volatile uint32_t g_led2_timer;
extern volatile uint32_t g_led3_off_interval;
extern volatile uint32_t g_led3_on_interval;
extern volatile uint16_t g_led3_flag;
extern volatile uint32_t g_led3_timer;

struct Command parse_command();
void execute_command(struct Command* command_ptr);
void execute_dac_set(struct Command* command_ptr);
void execute_led_static_set(struct Command* command_ptr);
void execute_led_blink_set(struct Command* command_ptr);
void execute_led_pwm_set(struct Command* command_ptr);
void execute_cool(struct Command* command_ptr);

void parse_cool_command(struct Command* command_ptr);
void parse_adc_command(struct Command* command_ptr);
void parse_led_command(struct Command* command_ptr);
void parse_dac_command(struct Command* command_ptr);
#endif